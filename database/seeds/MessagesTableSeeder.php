<?php

use Illuminate\Database\Seeder;
use App\Message;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $amount = 100;

        function randomFloat ($min,$max) {
            return ($min + lcg_value() * (abs($max - $min)));
        }

        for ($i = 0; $i < $amount; $i++) {
            $createdAt = $faker->dateTimeBetween($startDate = '-730 days', $endDate = '-3 days');
            $updatedAt = date_add($createdAt, date_interval_create_from_date_string('1 day'));
            Message::create([
                'email' => $faker->email,
                'name' => $faker->name,
                'content' => $faker->realText($maxNbChars = rand(100, 200)),
                'created_at' => $createdAt,
                'updated_at' => $updatedAt,
                'user_id' => rand(1,11)
            ]);
        }
    }
}
