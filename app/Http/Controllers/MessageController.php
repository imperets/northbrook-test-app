<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Message;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return Message::orderBy('created_at', 'DESC')->paginate(10);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
        	'name' => 'required|max:255',
          	'email' => 'required|email|max:255',
          	'content' => 'required'
        ]);

        $message = Message::create([
          	'name' => $validatedData['name'],
          	'email' => $validatedData['email'],
          	'content' => $validatedData['content'],
            'user_id' => auth()->user()->id
        ]);

        return response()->json($message, 201);
    }
    
    public function delete(Message $message)
    {
        if ($message['user_id'] === auth()->user()->id) {
            $message->delete();
            return response()->json(null, 204);
        }
        return 'Cannot delete message of another user';
    }
}