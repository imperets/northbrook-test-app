<?php use Illuminate\Support\Facades\Auth; ?>

<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="api-auth" data-api-token=<?php
            if(Auth::user()){
                echo json_encode(Auth::user()->api_token);
            }
        ?>>
    <meta name="user-id" data-user-id=<?php
            if(Auth::user()){
                echo json_encode(Auth::user()->id);
            }
        ?>>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Test App</title>
    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css') }}" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{ asset('https://use.fontawesome.com/releases/v5.0.6/css/all.css')  }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <script>
        function getApiToken() {
            var metas = document.getElementsByTagName('meta');
            for (var i=0; i<metas.length; i++) {
                if (metas[i].getAttribute("name") == "api-auth") {
                    return metas[i].getAttribute("data-api-token");
                }
            }
            return "";
        }
        apiToken = getApiToken();

        function getUserId() {
            var metas = document.getElementsByTagName('meta');
            for (var i=0; i < metas.length; i++) {
                if (metas[i].getAttribute("name") == "user-id") {
                    return parseInt(metas[i].getAttribute("data-user-id"));
                }
            }
            return;
        }
        userId = getUserId();
    </script>
    @yield('content')
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
