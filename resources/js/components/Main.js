import React from 'react'
import ReactDOM from 'react-dom'
import Messages from './Messages'
import Paginator from './Paginator'
import Form from './Form'
import Api from '../Api'

class Main extends React.Component {

    constructor(props){
        super(props);
        this.state = {};
    }

    getBatch(pageNumber){
        Api.getBatch('messages',pageNumber)
            .then(response => {
                this.setState({
                    messages: response.data,
                    numOfPages: response.last_page,
                    currentPage: response.current_page
                });
            })
    }

    componentDidMount(){
        this.getBatch(1);
    }

    render(){
        return(
            <div>
                <Form
                    getBatch={pageNumber => this.getBatch(pageNumber)}
                    userId={window.userId}
                />
                <Messages
                    getBatch={pageNumber => this.getBatch(pageNumber)}
                    data={this.state.messages}
                    userId={window.userId}
                    numOfPages={this.state.numOfPages}
                    currentPage={this.state.currentPage}
                />
                <Paginator
                    getBatch={pageNumber => this.getBatch(pageNumber)}
                    numOfPages={this.state.numOfPages}
                    currentPage={this.state.currentPage}
                />
            </div>
        )
    }

}

ReactDOM.render(<Main />, document.getElementById('app'));