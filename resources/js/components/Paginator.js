import React from 'react'

class Paginator extends React.Component {

    render(){
        const numOfPages = this.props.numOfPages;
        const currentPage = this.props.currentPage;
        return(
            <div className="col-md-6 offset-md-3">
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className="page-item">
                            <a className="page-link"
                               aria-label="Previous"
                               onClick={() => this.props.getBatch(currentPage - 1)}>
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                        {Array.from(Array(numOfPages), (e, i) => (
                             <li key={i}
                                 className={currentPage === i + 1 ? "page-item active" : "page-item"}>
                                 <a className="page-link btn"
                                    type="button"
                                    onClick={() => this.props.getBatch(i + 1)}>{i + 1}</a>
                             </li>
                        ))}
                        <li className="page-item">
                            <a className="page-link"
                               aria-label="Next"
                               onClick={() => currentPage < numOfPages
                                   ? this.props.getBatch(currentPage + 1) : ''}>
                                <span aria-hidden="true">&raquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }

}

export default Paginator;