import React from 'react'
import Api from '../Api'
import moment from 'moment'

class Messages extends React.Component {

    deleteMessage(id, currentPage){
        if (confirm("Are you sure?")) {
            Api.deleteOne('message',id)
                .then(() => this.props.getBatch(currentPage));
        }
    }

    render(){
        const data = this.props.data;
        const userId = this.props.userId;
        return(
            <div className="col-md-6 offset-md-3">
                <h1>Messages</h1>
                {data ? data.map(message => (
                    <div key={message.id} className="row">
                        <div className="container-fluid">
                            <div className="card">
                                <div className="card-body">
                                    <small>By <u>{message.name}</u>
                                        {message.user_id === userId ? <span> (you)</span> : ''} on <u>{moment(message.created_at).format('D MMMM YYYY HH:mm')}</u>
                                    </small>
                                    {message.user_id === userId
                                        ? <button className="btn btn-sm btn-danger float-right"
                                                  onClick={() => this.deleteMessage(message.id, this.props.currentPage)}>Delete</button>
                                        : <div />
                                    }
                                    <div>
                                        <p className="card-text">{message.content}</p>
                                    </div>
                                </div>
                            </div>
                        </div>&nbsp;
                    </div>
                )) : <div />}
            </div>
        )
    }

}

export default Messages;