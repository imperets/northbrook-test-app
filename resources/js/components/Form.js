import React from 'react'
import Api from '../Api'

class Form extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            email: '', name: '', content: '',
            user_id: props.userId
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event){
        this.setState({
            [event.target.name]: event.target.value,
        });
    }

    handleSubmit(){
        Api.addOne('messages', this.state)
            .then(() => this.props.getBatch(1))
            .then(() => this.setState({email: '', name: '', content: ''}));
    }

    render(){
        return(
            <div className="col-md-6 offset-md-3">
                <form>
                    <div className="form-group">
                        <label htmlFor="emailInput">Email address <small>(required)</small></label>
                        <input required
                               type="email"
                               className="form-control"
                               id="emailInput"
                               placeholder="e.g. name@example.com"
                               name="email"
                               value={this.state.email}
                               onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="nameInput">Name <small>(required)</small></label>
                        <input required
                               type="text"
                               className="form-control"
                               id="nameInput"
                               placeholder="e.g. John Doe"
                               name="name"
                               value={this.state.name}
                               onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group">
                        <label htmlFor="messageContent">Message <small>(required)</small></label>
                        <textarea required
                                  className="form-control"
                                  id="messageContent"
                                  rows="4"
                                  name="content"
                                  placeholder="Type your message here"
                                  value={this.state.content}
                                  onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group text-right">
                        <button type="button"
                                className="btn btn-primary"
                                onClick={this.handleSubmit}>Post</button>
                    </div>
                </form>
            </div>
        )
    }

}

export default Form;