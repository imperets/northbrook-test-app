import jQuery from 'jquery';

class Api {

    static getBatch(context,pageNumber){
        return jQuery.ajax({
            url: `/api/v1/${context}?page=${pageNumber}`,
            dataType: 'json',
            headers: {
                'Authorization': `Bearer ${window.apiToken}`
            },
            success: function(data){
                return data;
            },
            error: function(xhr, status, err){
                alert(err.toString());
            }
        });
    }

    static addOne(context,data){
        return jQuery.ajax({
            method: 'POST',
            dataType: 'json',
            url: `/api/v1/${context}`,
            headers: {
                'Authorization': `Bearer ${window.apiToken}`
            },
            data: data,
            success: function(data){
                return data;
            },
            error: function(xhr, status, err) {
                alert("Form is invalid. Make sure all fields are filled in and email address is correct.");
            }
        })
    }

    static deleteOne(context,id){
        return jQuery.ajax({
            method: 'DELETE',
            url: `/api/v1/${context}/${id}`,
            headers: {
                'Authorization': `Bearer ${window.apiToken}`
            },
            success: function(data){
                return data;
            },
            error: function(xhr, status, err){
                alert(err.toString());
            }
        });
    }

}

export default Api;